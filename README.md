puzzles
=======

Implementations of puzzles in different languages.

Notes
=======

The Rust implementation is old.  The syntax is no longer valid.  Someone needs to update it.


The C implementation is slower than pypy currently.  I suspect this is due to the number of deep copies performed compared to the other implemenations' use of references.
